import React, { useState } from "react";
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import ProfileCard from '../ProfileCard';
import ProfileInputModal from '../Modal/ProfileInputModal';

const Editor = props => {
    const [editorState, setEditorState] = useState({
        updatedText: '',
    });

    const [modalState, setModalState] = useState({show: false});
    
    const showModalHandler = () => {
        setModalState({show: !modalState.show});
    };

    const modules = {
        toolbar: {
            container: "#toolbar"
        },
        clipboard: {
            matchVisual: false,
        }
    };

    const formats = [
        "header",
        "font",
        "size",
        "bold",
        "italic",
        "underline",
        "strike",
        "blockquote",
        "list",
        "bullet",
        "indent",
        "link",
        "image",
        "color"
    ];
    
    return (
        <div>
            <ProfileCard click={showModalHandler} />
            <ReactQuill 
                theme="snow" 
                value={editorState} 
                onChange={setEditorState}
                modules={modules}
                formats={formats}
            />
            <ProfileInputModal show={modalState.show} click={showModalHandler}/>
        </div>
    );
}
export default Editor;