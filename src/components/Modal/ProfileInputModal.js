import React, { useState } from "react";
import { Modal, Button, Form } from "react-bootstrap";

const ProfileInputModal = (props) => {
   const myRef = React.createRef();

   return (
      <Modal animation={false} show={props.show === true} onHide={props.click}>
         <Modal.Header>
            <Modal.Title>Insert Profile</Modal.Title>
         </Modal.Header>
         <Modal.Body>
         <Form>
            <Form.Group controlId="name">
            <Form.Label>Name</Form.Label>
               <Form.Control type="text" placeholder="Enter name" />
            </Form.Group>
            <Form.Group controlId="description">
               <Form.Label>Description</Form.Label>
               <Form.Control as="textarea" rows={3} />
            </Form.Group>
         </Form>
         </Modal.Body>
         <Modal.Footer>
            <Button variant="secondary" onClick={props.click}>Close</Button>
            <Button variant="primary">Insert</Button>
         </Modal.Footer>
      </Modal>
   );
};

export default ProfileInputModal;