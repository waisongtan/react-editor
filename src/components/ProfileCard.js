import React, { useState } from "react";
import 'react-quill/dist/quill.snow.css';

const ProfileCard = props => {
    return (
        <div id="toolbar">
            <button className="ql-showModalHandler" onClick={props.click} data-toggle="modal" data-target="#myModal">
                <i className="far fa-address-card"></i>
            </button>
        </div>
    );
}
export default ProfileCard;