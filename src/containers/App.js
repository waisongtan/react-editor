import './App.css';
import 'react-quill/dist/quill.snow.css';
import Editor from '../components/Editor/Editor';

function App() {
  return (
    <div className="App">
      <header className="bg-primary text-center py-5 mb-4">
        <div className="container">
          <h1 className="font-weight-light text-white">Editor</h1>
        </div>
      </header>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="card border-0 shadow">
              <Editor />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
